package blue.soft.clientservice;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.DslPart;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import blue.soft.clientservice.dto.AccountDTO;
import blue.soft.clientservice.dto.RoleDTO;
import blue.soft.clientservice.dto.UserDTO;
import blue.soft.clientservice.dto.UserResponse;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.*;

import static io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(PactConsumerTestExt.class)
class UserConsumerPactTest {

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact getAllUsers(PactDslWithProvider builder) {
        UserResponse userResponse = getUserResponseWithListUsers();
        DslPart body = prepareDslPartFromUserResponse(userResponse);

        return builder.given("all users")
                .uponReceiving("Get all users")
                .path("/api/user/")
                .method("GET")
                .willRespondWith()
                .status(200)
                .body(body)
                .toPact();
    }

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact getOneUser(PactDslWithProvider builder) {
        UserResponse userResponse = getUserResponseWithOneUser();
        DslPart body = prepareDslPartFromUserResponse(userResponse);

        return builder.given("user with id 611f61b328d43a158c058d53")
                .uponReceiving("Get user by id")
                .method("GET")
                .path("/api/user/611f61b328d43a158c058d53")
                .willRespondWith()
                .status(200)
                .body(body)
                .toPact();
    }


    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact getUserByLastname(PactDslWithProvider builder) {
        UserResponse userResponse = getUserResponseWithMessage("Failed to complete get user by lastname request.");
        DslPart body = prepareDslPartFromUserResponse(userResponse);

        return builder.given("user with lastname maklowicz")
                .uponReceiving("Get user by lastname - with error")
                .method("GET")
                .path("/api/user/byLastname/maklowicz")
                .willRespondWith()
                .status(500)
                .body(body)
                .toPact();
    }

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact createUser(PactDslWithProvider builder) {
        UserDTO user = getOneUser();
        DslPart bodyRequest = prepareDslPartFromUserDTO(user);

        UserResponse userResponse = getUserResponseWithMessage("User successfully registered.");
        DslPart bodyResponse = prepareDslPartFromUserResponse(userResponse);

        return builder
                .given("new user")
                .uponReceiving("Add new user")
                .path("/api/user/")
                .method("POST")
                .body(bodyRequest)
                .willRespondWith()
                .status(201)
                .body(bodyResponse)
                .toPact();
    }

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact createUserWithError(PactDslWithProvider builder) {
        UserDTO user = getOneUser();
        DslPart bodyRequest = prepareDslPartFromUserDTO(user);

        UserResponse userResponse = getUserResponseWithMessage("Failed to add user request.");
        DslPart bodyResponse = prepareDslPartFromUserResponse(userResponse);

        return builder
                .given("new user with error")
                .uponReceiving("Add new user with error")
                .path("/api/user/")
                .method("POST")
                .body(bodyRequest)
                .willRespondWith()
                .status(500)
                .body(bodyResponse)
                .toPact();
    }

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact deleteUser(PactDslWithProvider builder) {
        UserResponse userResponse = getUserResponseWithMessage("User successfully deleted.");
        DslPart bodyResponse = prepareDslPartFromUserResponse(userResponse);

        return builder
                .given("delete user")
                .uponReceiving("Delete user")
                .path("/api/user/611f61b328d43a158c058d53")
                .method("DELETE")
                .willRespondWith()
                .status(200)
                .body(bodyResponse)
                .toPact();
    }

    @Pact(consumer = "UserClient", provider = "ConsumerController")
    RequestResponsePact deleteUserWithError(PactDslWithProvider builder) {
        UserResponse userResponse = getUserResponseWithMessage("Failed to delete user request.");
        DslPart bodyResponse = prepareDslPartFromUserResponse(userResponse);

        return builder
                .given("delete user with error")
                .uponReceiving("Delete user with error")
                .path("/api/user/611f61b328d43a158c058d53")
                .method("DELETE")
                .willRespondWith()
                .status(500)
                .body(bodyResponse)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "getAllUsers")
    void shouldReturnListUserIfGetRequestIsCorrect(MockServer mockServer) {
        String url = mockServer.getUrl() + "/api/user/";

        ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, null, String.class);

        LinkedHashMap firstUserJsonArray = JsonPath.read(response.getBody(), "$.users[0]");
        LinkedHashMap firstUserAccount = (LinkedHashMap) firstUserJsonArray.get("account");
        JSONArray firstUserRoles = (JSONArray) firstUserAccount.get("roles");

        LinkedHashMap secondUserJsonArray = JsonPath.read(response.getBody(), "$.users[1]");
        LinkedHashMap secondUserAccount = (LinkedHashMap) secondUserJsonArray.get("account");
        JSONArray secondUserRoles = (JSONArray) firstUserAccount.get("roles");

        assertEquals("611f61b328d43a158c058d53", firstUserJsonArray.get("id"));
        assertEquals("Romek", firstUserJsonArray.get("firstname"));
        assertEquals("Grzegorczyk", firstUserJsonArray.get("lastname"));
        assertEquals("test123@gmail.com", firstUserJsonArray.get("emailAddress"));
        assertEquals("test123", firstUserAccount.get("username"));
        assertEquals("123123", firstUserAccount.get("password"));
        assertEquals("WRITE_PRIVILEGE", firstUserRoles.get(0));

        assertEquals("611f61b328d43a158c058d54", secondUserJsonArray.get("id"));
        assertEquals("Marek", secondUserJsonArray.get("firstname"));
        assertEquals("Maklowicz", secondUserJsonArray.get("lastname"));
        assertEquals("test321@wp.pl", secondUserJsonArray.get("emailAddress"));
        assertEquals("marek123", secondUserAccount.get("username"));
        assertEquals("Qwerty321", secondUserAccount.get("password"));
        assertEquals("WRITE_PRIVILEGE", secondUserRoles.get(0));
    }

    @Test
    @PactTestFor(pactMethod = "getOneUser")
    void shouldReturnUserIfGetRequestIsCorrect(MockServer mockServer) {
        String url = mockServer.getUrl() + "/api/user/611f61b328d43a158c058d53";

        ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, null, String.class);
        LinkedHashMap firstUserJsonArray = JsonPath.read(response.getBody(), "$.users[0]");

        assertEquals("Romek", firstUserJsonArray.get("firstname"));
        assertEquals("Grzegorczyk", firstUserJsonArray.get("lastname"));
        assertEquals("test123@gmail.com", firstUserJsonArray.get("emailAddress"));
    }

    @Test
    @PactTestFor(pactMethod = "getUserByLastname")
    void shouldReturnExceptionIfGetRequestReturnError(MockServer mockServer) {
        RestTemplate restTemplate = new RestTemplate();
        String url = mockServer.getUrl() + "/api/user/byLastname/maklowicz";

        assertThrows(HttpServerErrorException.class, () -> restTemplate.exchange(url, HttpMethod.GET, null, String.class));
    }

    @Test
    @PactTestFor(pactMethod = "createUser")
    void shouldReturnMessageIfPostRequestIsCorrect(MockServer mockServer) {
        String url = mockServer.getUrl() + "/api/user/";

        String resultMessage = postRequest(url);

        assertEquals("User successfully registered.", resultMessage);
    }

    @Test
    @PactTestFor(pactMethod = "createUserWithError")
    void shouldReturnExceptionIfPostRequestReturnError(MockServer mockServer) {
        String url = mockServer.getUrl() + "/api/user/";

        assertThrows(HttpServerErrorException.class, () -> postRequest(url));
    }


    @Test
    @PactTestFor(pactMethod = "deleteUser")
    void shouldReturnMessageIfDeleteRequestIsCorrect(MockServer mockServer) {
        RestTemplate restTemplate = new RestTemplate();
        String url = mockServer.getUrl() + "/api/user/611f61b328d43a158c058d53";

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        String resultMessage = JsonPath.read(response.getBody(), "$.message");

        assertEquals("User successfully deleted.", resultMessage);
    }

    @Test
    @PactTestFor(pactMethod = "deleteUserWithError")
    void shouldReturnExceptionIfDeleteRequestReturnError(MockServer mockServer) {
        RestTemplate restTemplate = new RestTemplate();
        String url = mockServer.getUrl() + "/api/user/611f61b328d43a158c058d53";

        assertThrows(HttpServerErrorException.class, () -> restTemplate.exchange(url, HttpMethod.DELETE, null, String.class));
    }

    private UserResponse getUserResponseWithMessage(String message) {
        return UserResponse.builder().message(message).users(new ArrayList<>()).build();
    }

    private UserDTO getOneUser() {
        AccountDTO firstUserAccount = AccountDTO.builder().username("test123").password("123123").roles(Collections.singletonList(RoleDTO.WRITE_PRIVILEGE)).build();

        return UserDTO.builder().firstname("Romek").lastname("Grzegorczyk").emailAddress("test123@gmail.com").account(firstUserAccount).build();
    }

    private UserResponse getUserResponseWithOneUser() {
        List<UserDTO> users = List.of(getOneUser());

        return UserResponse.builder().message("").users(users).build();
    }

    private UserResponse getUserResponseWithListUsers() {
        AccountDTO firstUserAccount = AccountDTO.builder().username("test123").password("123123").roles(Collections.singletonList(RoleDTO.WRITE_PRIVILEGE)).build();
        UserDTO firstUser = UserDTO.builder().id("611f61b328d43a158c058d53").firstname("Romek").lastname("Grzegorczyk").emailAddress("test123@gmail.com").account(firstUserAccount).build();

        AccountDTO secondUserAccount = AccountDTO.builder().username("marek123").password("Qwerty321").roles(Collections.singletonList(RoleDTO.WRITE_PRIVILEGE)).build();
        UserDTO secondUser = UserDTO.builder().id("611f61b328d43a158c058d54").firstname("Marek").lastname("Maklowicz").emailAddress("test321@wp.pl").account(secondUserAccount).build();

        List<UserDTO> users = List.of(firstUser, secondUser);

        return UserResponse.builder().message("").users(users).build();
    }

    private String postRequest(String url) {
        RestTemplate restTemplate = new RestTemplate();

        Gson gson = new Gson();
        String userRequest = gson.toJson(getOneUser());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));

        HttpEntity<String> entity = new HttpEntity<String>(userRequest, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

        return JsonPath.read(response.getBody(), "$.message");
    }

    private DslPart prepareDslPartFromUserDTO(UserDTO userDTO) {
        return newJsonBody((root) -> {
            root.stringType("id", userDTO.getId());
            root.stringType("firstname", userDTO.getFirstname());
            root.stringType("lastname", userDTO.getLastname());
            root.stringType("emailAddress", userDTO.getEmailAddress());
            root.object("account", accountObject -> {
                accountObject.stringType("username", userDTO.getAccount().getUsername());
                accountObject.stringType("password", userDTO.getAccount().getPassword());
                accountObject.array("roles", rolesJsonArray -> {
                    userDTO.getAccount().getRoles().forEach(roleDTO -> {
                        rolesJsonArray.stringType(roleDTO.toString());
                    });
                });
            });
        }).build();
    }

    private DslPart prepareDslPartFromUserResponse(UserResponse response) {
        return newJsonBody((root) -> {
            root.stringValue("message", response.getMessage());
            root.array("users", lambdaDslJsonArray -> {
                response.getUsers().forEach(userDTO -> {
                    lambdaDslJsonArray.object(lambdaDslObject -> {
                        lambdaDslObject.stringType("id", userDTO.getId());
                        lambdaDslObject.stringType("firstname", userDTO.getFirstname());
                        lambdaDslObject.stringType("lastname", userDTO.getLastname());
                        lambdaDslObject.stringType("emailAddress", userDTO.getEmailAddress());
                        lambdaDslObject.object("account", accountObject -> {
                            accountObject.stringType("username", userDTO.getAccount().getUsername());
                            accountObject.stringType("password", userDTO.getAccount().getPassword());
                            accountObject.array("roles", rolesJsonArray -> {
                                userDTO.getAccount().getRoles().forEach(roleDTO -> {
                                    rolesJsonArray.stringType(roleDTO.toString());
                                });
                            });
                        });
                    });
                });
            });
        }).build();
    }
}