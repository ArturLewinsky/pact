package blue.soft.clientservice.client;

import blue.soft.clientservice.dto.UserDTO;
import blue.soft.clientservice.dto.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "server-service")
public interface UserClient {

    @GetMapping("/api/user/")
    UserResponse getAllUsers();

    @GetMapping("/api/user/{id}")
    UserResponse getUserById(@PathVariable String id);

    @GetMapping("/api/user/byLastname/{lastname}")
    UserResponse getUserByLastname(@PathVariable String lastname);

    @PostMapping("/api/user/")
    UserResponse addUser(@RequestBody UserDTO userDTO);

    @PostMapping("/api/user/witherror")
    UserResponse addUserWithError(@PathVariable UserDTO userDTO);

    @DeleteMapping("/api/user/{id}")
    UserResponse deleteUser(@PathVariable String id);

    @DeleteMapping("/api/user/witherror/{id}")
    UserResponse deleteUserWithError(@PathVariable String id);
}

