package blue.soft.clientservice.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserResponse {
    String message;
    List<UserDTO> users;
}
