package blue.soft.clientservice.dto;

public enum RoleDTO {
    WRITE_PRIVILEGE, READ_PRIVILEGE;
}
