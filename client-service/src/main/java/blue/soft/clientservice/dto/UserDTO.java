package blue.soft.clientservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UserDTO {
    @JsonIgnore
    private String id;
    private String firstname;
    private String lastname;
    private String emailAddress;
    private AccountDTO account;
}
