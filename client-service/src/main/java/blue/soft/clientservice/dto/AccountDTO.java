package blue.soft.clientservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class AccountDTO {
    private String username;
    private String password;
    List<RoleDTO> roles;
}
