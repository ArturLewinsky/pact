package blue.soft.clientservice.controllers;

import blue.soft.clientservice.client.UserClient;
import blue.soft.clientservice.dto.UserDTO;
import blue.soft.clientservice.dto.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreaker;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.function.Supplier;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/user")
public class UserController {

    private final UserClient userClient;
    private final Resilience4JCircuitBreakerFactory circuitBreakerFactory;

    @GetMapping("/")
    public UserResponse getAllUsers() {
        Resilience4JCircuitBreaker userCircuitBreaker = circuitBreakerFactory.create("user");
        Supplier<UserResponse> userCircuitBreakerSupplier = userClient::getAllUsers;

        return userCircuitBreaker.run(userCircuitBreakerSupplier, throwable -> handlerErrorUserCircuitBreakerSupplier());
    }

    @GetMapping("/{id}")
    public UserResponse getUserById(@PathVariable String id) {
        Resilience4JCircuitBreaker userCircuitBreaker = circuitBreakerFactory.create("userById");
        Supplier<UserResponse> userCircuitBreakerSupplier = () -> userClient.getUserById(id);

        return userCircuitBreaker.run(userCircuitBreakerSupplier, throwable -> handlerErrorUserCircuitBreakerSupplier());
    }

    @GetMapping("/byLastname/{lastname}")
    public UserResponse getUserByLastname(@PathVariable String lastname) {
        Resilience4JCircuitBreaker userCircuitBreaker = circuitBreakerFactory.create("userByName");
        Supplier<UserResponse> userCircuitBreakerSupplier = () -> userClient.getUserByLastname(lastname);

        return userCircuitBreaker.run(userCircuitBreakerSupplier, throwable -> handlerErrorUserCircuitBreakerSupplier());
    }

    @PostMapping("/")
    public UserResponse addUser(@RequestBody UserDTO userDTO) {
        Resilience4JCircuitBreaker userCircuitBreaker = circuitBreakerFactory.create("addUser");
        Supplier<UserResponse> userCircuitBreakerSupplier = () -> userClient.addUser(userDTO);

        return userCircuitBreaker.run(userCircuitBreakerSupplier, throwable -> handlerErrorUserCircuitBreakerSupplier());
    }

    @DeleteMapping("/{id}")
    public UserResponse deleteUser(@PathVariable String id) {
        Resilience4JCircuitBreaker userCircuitBreaker = circuitBreakerFactory.create("deleteUser");
        Supplier<UserResponse> userCircuitBreakerSupplier = () -> userClient.deleteUser(id);

        return userCircuitBreaker.run(userCircuitBreakerSupplier, throwable -> handlerErrorUserCircuitBreakerSupplier());
    }

    private UserResponse handlerErrorUserCircuitBreakerSupplier() {
        return UserResponse.builder().message("Error").build();
    }
}
