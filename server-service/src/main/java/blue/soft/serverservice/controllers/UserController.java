package blue.soft.serverservice.controllers;

import blue.soft.serverservice.dto.UserResponse;
import blue.soft.serverservice.models.User;
import blue.soft.serverservice.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Api(value = "User api")
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/user")
public class UserController {
    private final UserService userService;

    @ApiOperation(value = "Get all users")
    @GetMapping("/")
    public ResponseEntity<UserResponse> getAllUsers() {
        try {
            List<User> users = userService.getAllUsers();

            if (users.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(new UserResponse(users), HttpStatus.OK);
        } catch (Exception e) {
            String safeErrorMessage = "Failed to complete get all users request.";

            return new ResponseEntity<>(new UserResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get user by id")
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable String id) {
        try {
            Optional<User> user = userService.getUserById(id);

            return user
                    .map(value -> new ResponseEntity<>(new UserResponse(value), HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NO_CONTENT));
        } catch (Exception e) {
            String safeErrorMessage = "Failed to complete get user by id request.";

            return new ResponseEntity<>(new UserResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get user by lastname - with error")
    @GetMapping("/byLastname/{lastname}")
    public ResponseEntity<UserResponse> getUserByLastname(@PathVariable String lastname) {
        try {
            userService.getUserByLastName(lastname);

            throw new Exception();
        } catch (Exception e) {
            String safeErrorMessage = "Failed to complete get user by lastname request.";

            return new ResponseEntity<>(new UserResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Add user")
    @PostMapping("/")
    public ResponseEntity<UserResponse> addUser(@RequestBody User user) {
        try {
            userService.addUser(user);

            return new ResponseEntity<>(new UserResponse("User successfully registered."), HttpStatus.CREATED);
        } catch (Exception e) {
            String safeErrorMessage = "Failed to add user request.";

            return new ResponseEntity<>(new UserResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Delete user")
    @DeleteMapping("/{id}")
    public ResponseEntity<UserResponse> deleteUser(@PathVariable String id) {
        try {
            userService.deleteUser(id);

            return new ResponseEntity<>(new UserResponse("User successfully deleted."), HttpStatus.OK);
        } catch (Exception e) {
            String safeErrorMessage = "Failed to delete user request.";

            return new ResponseEntity<>(new UserResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
