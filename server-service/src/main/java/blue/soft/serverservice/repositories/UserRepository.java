package blue.soft.serverservice.repositories;

import blue.soft.serverservice.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<List<User>> findByLastname(String lastname);
}
