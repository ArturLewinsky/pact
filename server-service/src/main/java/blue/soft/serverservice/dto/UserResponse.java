package blue.soft.serverservice.dto;

import blue.soft.serverservice.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class UserResponse {
    private List<User> users;
    private String message;

    public UserResponse(String message) {
        this.message = message;
        this.users = new ArrayList<>();
    }

    public UserResponse(List<User> users) {
        this.message = "";
        this.users = users;
    }

    public UserResponse(String message, User user) {
        this.message = message;
        this.users = new ArrayList<>();
        this.users.add(user);
    }

    public UserResponse(User user) {
        this.message = "";
        this.users = new ArrayList<>();
        this.users.add(user);
    }

    public List<User> getUsers() {
        return this.users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
