package blue.soft.serverservice;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.StateChangeAction;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.spring.junit5.PactVerificationSpringProvider;
import blue.soft.serverservice.models.Account;
import blue.soft.serverservice.models.Role;
import blue.soft.serverservice.models.User;
import blue.soft.serverservice.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@PactBroker
@Provider("ConsumerController")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserPactTest {

    @MockBean
    private UserService userService;

    @LocalServerPort
    private int port;

    @BeforeEach
    void before(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", port));
    }

    @TestTemplate
    @ExtendWith(PactVerificationSpringProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State(value = "all users", action = StateChangeAction.SETUP)
    void toGetListUserState() {
        List<User> users = getListUsers();
        when(userService.getAllUsers()).thenReturn(users);
    }

    @State(value = "user with id 611f61b328d43a158c058d53", action = StateChangeAction.SETUP)
    void toGetUserWithSetIdState() {
        User user = getOneUser();
        when(userService.getUserById("611f61b328d43a158c058d53")).thenReturn(Optional.of(user));
    }

    @State(value = "user with lastname maklowicz", action = StateChangeAction.SETUP)
    void toGetUserWithSetLastNameState() {
        User user = getOneUser();
        when(userService.getUserById("maklowicz")).thenReturn(Optional.of(user));
    }

    @State(value = {"new user", "new user with error"}, action = StateChangeAction.SETUP)
    public void toAddUserState() {
        User user = getOneUser();
        when(userService.addUser(any(User.class))).thenReturn(user);
    }


    @State(value = {"delete user", "delete user with error"}, action = StateChangeAction.SETUP)
    public void toDeleteUserState() {
        doNothing().when(userService).deleteUser(any());
    }

    private User getOneUser() {
        Account firstUserAccount = Account.builder().username("test123").password("123123").roles(Collections.singletonList(Role.WRITE_PRIVILEGE)).build();

        return User.builder().firstname("Romek").lastname("Grzegorczyk").emailAddress("test123@gmail.com").account(firstUserAccount).build();
    }

    private List<User> getListUsers() {
        Account firstUserAccount = Account.builder().username("test123").password("123123").roles(Collections.singletonList(Role.WRITE_PRIVILEGE)).build();
        User firstUser = User.builder().id("611f61b328d43a158c058d53").firstname("Romek").lastname("Grzegorczyk").emailAddress("test123@gmail.com").account(firstUserAccount).build();

        Account secondUserAccount = Account.builder().username("marek123").password("Qwerty321").roles(Collections.singletonList(Role.WRITE_PRIVILEGE)).build();
        User secondUser = User.builder().id("611f61b328d43a158c058d54").firstname("Marek").lastname("Makłowicz").emailAddress("test321@wp.pl").account(secondUserAccount).build();

        return List.of(firstUser, secondUser);
    }
}
